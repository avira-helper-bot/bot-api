package db

import (
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	bolt "go.etcd.io/bbolt"
)

var DB *bolt.DB

func init() {
	var err error
	DB, err = bolt.Open("database.db", 0600, nil)
	if err != nil {
		log.Fatal(err)
	}

	err = DB.Update(func(tx *bolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists([]byte("Lunchers"))
		if err != nil {
			return errors.Wrap(err, "unable to create the lunchers bucket")
		}
		return nil
	})
	if err != nil {
		log.Fatal(err)
	}
}

func CloseDB() {
	if err := DB.Close(); err != nil {
		log.Fatal(err)
	}
}
