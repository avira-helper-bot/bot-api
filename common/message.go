package common

type MessageFrom struct {
	ID    int  `json:"id"`
	IsBot bool `json:"is_bot"`

	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	UserName  string `json:"username"`

	LanguageCode string `json:"language_code"`
}

type MessageChat struct {
	ID   int    `json:"id"`
	Type string `json:"type"`

	// fields for the "private" type of the chat
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	UserName  string `json:"username"`

	// fields for the "group" type of the chat
	Title               string `json:"title"`
	AllMembersAreAdmins bool   `json:"all_members_are_administrators"`
}

type Message struct {
	ID int `json:"message_id"`

	Date int    `json:"date"`
	Text string `json:"text"`

	From MessageFrom `json:"from"`
	Chat MessageChat `json:"chat"`
}
