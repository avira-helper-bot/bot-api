package lunchers

import (
	"encoding/json"
	"github.com/pkg/errors"
	"gitlab.com/avira-helper-bot/bot-api/common"
	"gitlab.com/avira-helper-bot/bot-api/db"
	"gitlab.com/avira-helper-bot/bot-api/tg"
	bolt "go.etcd.io/bbolt"
	"strconv"
)

func ProcessLunchApply(message *common.Message) error {
	userJson, err := json.Marshal(message.From)
	if err != nil {
		return errors.Wrap(err, "unable to marshal user object")
	}

	err = db.DB.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("Lunchers"))
		err := b.Put(
			[]byte(strconv.Itoa(message.From.ID)),
			userJson,
		)
		if err != nil {
			return errors.Wrap(err, "unable to save the luncher")
		}
		return nil
	})
	if err != nil {
		return errors.Wrap(err, "unable to save the luncher")
	}

	_, err = tg.SendMessage(tg.SendMessageParams{
		ChatID:           message.Chat.ID,
		Text:             "<b>Thank you!</b> You've been added to the lunchers list.",
		ReplyToMessageID: message.ID,
	})
	if err != nil {
		return errors.Wrap(err, "unable to apply user on lunchers")
	}

	return nil
}