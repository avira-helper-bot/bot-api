package lunchers

import (
	"encoding/json"
	"fmt"
	"github.com/pkg/errors"
	"gitlab.com/avira-helper-bot/bot-api/common"
	"gitlab.com/avira-helper-bot/bot-api/db"
	"gitlab.com/avira-helper-bot/bot-api/tg"
	bolt "go.etcd.io/bbolt"
	"strings"
)

func ProcessLunchList(message *common.Message) error {
	names := []string{}

	err := db.DB.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("Lunchers"))
		c := b.Cursor()

		for k, v := c.First(); k != nil; k, v = c.Next() {
			user := common.MessageFrom{}
			err := json.Unmarshal(v, &user)
			if err != nil {
				return errors.Wrap(err, "unable to get the lunchers list")
			}
			names = append(
				names,
				strings.Trim(fmt.Sprintf("%s %s", user.FirstName, user.LastName), " "),
			)
		}

		return nil
	})
	if err != nil {
		return errors.Wrap(err, "unable to list the lunchers")
	}

	_, err = tg.SendMessage(tg.SendMessageParams{
		ChatID:           message.Chat.ID,
		Text:             fmt.Sprintf("<b>Lunchers list:</b>\n\n%s", strings.Join(names, "\n")),
		ReplyToMessageID: message.ID,
	})
	if err != nil {
		return errors.Wrap(err, "unable to apply user on lunchers")
	}

	return nil
}
