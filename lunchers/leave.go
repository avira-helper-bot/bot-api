package lunchers

import (
	"github.com/pkg/errors"
	"gitlab.com/avira-helper-bot/bot-api/common"
	"gitlab.com/avira-helper-bot/bot-api/db"
	"gitlab.com/avira-helper-bot/bot-api/tg"
	bolt "go.etcd.io/bbolt"
	"strconv"
)

func ProcessLunchLeave(message *common.Message) error {
	err := db.DB.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("Lunchers"))
		err := b.Delete(
			[]byte(strconv.Itoa(message.From.ID)),
		)
		if err != nil {
			return errors.Wrap(err, "unable to delete the luncher")
		}
		return nil
	})
	if err != nil {
		return errors.Wrap(err, "unable to delete the luncher")
	}

	_, err = tg.SendMessage(tg.SendMessageParams{
		ChatID:           message.Chat.ID,
		Text:             "<b>Thank you!</b> You've been removed from the lunchers list.",
		ReplyToMessageID: message.ID,
	})
	if err != nil {
		return errors.Wrap(err, "unable to apply user on lunchers")
	}

	return nil
}
