package route

import (
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	log "github.com/sirupsen/logrus"
	"gitlab.com/avira-helper-bot/bot-api/about"
	"gitlab.com/avira-helper-bot/bot-api/common"
	"gitlab.com/avira-helper-bot/bot-api/lunchers"
	"strings"
)

type webhookData struct {
	UpdateID int            `json:"update_id"`
	Message  common.Message `json:"message"`
}

func processGroupMessage(message *common.Message) {
	var err error
	switch {
	case strings.HasPrefix(message.Text, "/lunch_apply"):
		err = lunchers.ProcessLunchApply(message)
		break;
	case strings.HasPrefix(message.Text, "/lunch_leave"):
		err = lunchers.ProcessLunchLeave(message)
		break;
	case strings.HasPrefix(message.Text, "/lunch_list"):
		err = lunchers.ProcessLunchList(message)
		break;
	case strings.HasPrefix(message.Text, "/about"):
		err = about.SendAboutInfo(message.Chat.ID, message.ID)
		break;
	}
	if err != nil {
		log.Error(err)
	}
}

func webhookHandler(c *gin.Context) {
	data := webhookData{}
	if err := c.ShouldBindBodyWith(&data, binding.JSON); err != nil {
		log.Error(err)
	} else {
		log.Infof("%+v", data)
		if data.Message.Chat.Type == "supergroup" || data.Message.Chat.Type == "group" {
			processGroupMessage(&data.Message)
		}
	}
	c.JSON(200, gin.H{"result": true})
}

func initWebhook(r *gin.Engine) {
	r.POST("/webhook", webhookHandler)
}
