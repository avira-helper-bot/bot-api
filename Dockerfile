FROM golang:latest
LABEL maintainer="resurtm <resurtm@gmail.com>"
WORKDIR /app
COPY go.mod go.sum ./
RUN go mod download
COPY . .
RUN go build -o bot-api .
EXPOSE 8080
CMD ["./bot-api"]
