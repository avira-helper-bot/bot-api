package about

import (
	"github.com/pkg/errors"
	"gitlab.com/avira-helper-bot/bot-api/tg"
)

func SendAboutInfo(chatID int, replyToMessageID int) error {
	_, err := tg.SendMessage(tg.SendMessageParams{
		ChatID:           chatID,
		Text:             "More info here:\n\nhttps://gitlab.com/avira-helper-bot/",
		ReplyToMessageID: replyToMessageID,
	})
	if err != nil {
		return errors.Wrap(err, "unable to send the about message")
	}

	return nil
}
