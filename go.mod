module gitlab.com/avira-helper-bot/bot-api

go 1.12

require (
	github.com/gin-gonic/gin v1.4.0
	github.com/imroc/req v0.2.4
	github.com/mattn/go-isatty v0.0.8 // indirect
	github.com/pkg/errors v0.8.1
	github.com/sirupsen/logrus v1.4.2
	go.etcd.io/bbolt v1.3.3
	golang.org/x/net v0.0.0-20190813141303-74dc4d7220e7 // indirect
	golang.org/x/sys v0.0.0-20190813064441-fde4db37ae7a // indirect
)
