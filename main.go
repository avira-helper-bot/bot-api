package main // import "gitlab.com/avira-helper-bot/bot-api"

import (
	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	"gitlab.com/avira-helper-bot/bot-api/config"
	"gitlab.com/avira-helper-bot/bot-api/db"
	"gitlab.com/avira-helper-bot/bot-api/route"
	"gitlab.com/avira-helper-bot/bot-api/tg"
)

func setupTelegram() {
	info, err := tg.GetMe()
	if err != nil {
		log.Fatal(err)
	}
	log.Infof("bot info: %+v", info)

	if err = tg.DeleteWebhook(); err != nil {
		log.Fatal(err)
	}
	if err = tg.SetWebhook(config.WebhookUrl); err != nil {
		log.Fatal(err)
	}
}

func main() {
	defer db.CloseDB()

	setupTelegram()

	r := gin.Default()
	route.New(r)
	if err := r.Run(config.ListenAddr); err != nil {
		log.Fatal(err)
	}
}
