package config

import (
	log "github.com/sirupsen/logrus"
	"os"
)

var WebhookUrl string
var ListenAddr string
var BotApiToken string

func init() {
	WebhookUrl = os.Getenv("WEBHOOK_URL")
	if WebhookUrl == "" {
		log.Fatalf("config: WEBHOOK_URL environment variable is required")
	}

	ListenAddr = os.Getenv("LISTEN_ADDR")
	if ListenAddr == "" {
		log.Fatalf("config: LISTEN_ADDR environment variable is required")
	}

	BotApiToken = os.Getenv("BOT_API_TOKEN")
	if BotApiToken == "" {
		log.Fatalf("config: BOT_API_TOKEN environment variable is required")
	}
}
