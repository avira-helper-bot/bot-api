install:
	go mod download

build:
	go build -o bot-api .

clean:
	rm -rfv ./bot-api

dev:
	LISTEN_ADDR=:9100 modd

lint:
	golangci-lint run

format:
	gofmt -l -s -w .

vet:
	go vet .
