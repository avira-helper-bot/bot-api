package tg

import (
	"fmt"
	"github.com/imroc/req"
	"github.com/pkg/errors"
)

type setWebhookResponse struct {
	OK          bool   `json:"ok"`
	Result      bool   `json:"result"`
	ErrorCode   int    `json:"error_code"`
	Description string `json:"description"`
}

func SetWebhook(url string) error {
	resp, err := req.Post(
		getTelegramUrl("setWebhook"),
		req.Param{"url": url},
	)
	if err != nil {
		return errors.Wrap(err, "setwebhook: post request failed")
	}

	data := setWebhookResponse{}
	err = resp.ToJSON(&data)
	if err != nil {
		return errors.Wrap(err, "setwebhook: to json failed")
	}
	if !data.OK {
		return errors.New(fmt.Sprintf(
			"setwebhook: error_code=%d, description='%s'",
			data.ErrorCode,
			data.Description,
		))
	}

	return nil
}
