package tg

import (
	"fmt"
	"github.com/imroc/req"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	"gitlab.com/avira-helper-bot/bot-api/common"
)

type messageData struct {
	OK          bool           `json:"ok"`
	Result      common.Message `json:"result"`
	Description string         `json:"description"`
}

type SendMessageParams struct {
	ChatID           int
	Text             string
	ParseMode        string
	ReplyToMessageID int
}

func SendMessage(params SendMessageParams) (*common.Message, error) {
	if params.ChatID == 0 {
		return nil, errors.New("sendmessage: chat id parameter is required")
	}
	if params.Text == "" {
		return nil, errors.New("sendmessage: text parameter is required")
	}
	if params.ParseMode == "" {
		params.ParseMode = "HTML"
	}
	log.Infof("sendmessage: sending the message with the following params: %+v", params)
	return sendMessage(params.ChatID, params.Text, params.ParseMode, params.ReplyToMessageID)
}

func sendMessage(chatID int, text string, parseMode string, replyToMessageID int) (*common.Message, error) {
	params := req.Param{
		"chat_id":    chatID,
		"text":       text,
		"parse_mode": parseMode,
	}
	if replyToMessageID != 0 {
		params["reply_to_message_id"] = replyToMessageID
	}
	resp, err := req.Post(getTelegramUrl("sendMessage"), params)
	if err != nil {
		return nil, errors.Wrap(err, "sendmessage: post request failed")
	}

	data := messageData{}
	err = resp.ToJSON(&data)
	if err != nil {
		return nil, errors.Wrap(err, "sendmessage: to json failed")
	}
	if !data.OK {
		return nil, errors.New(fmt.Sprintf("sendmessage: description='%s'", data.Description))
	}

	return &data.Result, nil
}
