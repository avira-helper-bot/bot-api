package tg

import (
	"fmt"
	"github.com/imroc/req"
	"github.com/pkg/errors"
)

type getMeResponseResult struct {
	ID        int    `json:"id"`
	IsBot     bool   `json:"is_bot"`
	FirstName string `json:"first_name"`
	UserName  string `json:"username"`
}

type getMeResponse struct {
	OK          bool                `json:"ok"`
	Result      getMeResponseResult `json:"result"`
	Description string              `json:"description"`
}

func GetMe() (*getMeResponseResult, error) {
	resp, err := req.Post(getTelegramUrl("getMe"))
	if err != nil {
		return nil, errors.Wrap(err, "getme: post request failed")
	}

	data := getMeResponse{}
	err = resp.ToJSON(&data)
	if err != nil {
		return nil, errors.Wrap(err, "getme: to json failed")
	}
	if !data.OK {
		return nil, errors.New(fmt.Sprintf("getme: description='%s'", data.Description))
	}

	return &data.Result, nil
}
