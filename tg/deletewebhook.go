package tg

import (
	"fmt"
	"github.com/imroc/req"
	"github.com/pkg/errors"
)

type deleteWebhookResponse struct {
	OK          bool   `json:"ok"`
	Result      bool   `json:"result"`
	Description string `json:"description"`
}

func DeleteWebhook() error {
	resp, err := req.Post(getTelegramUrl("deleteWebhook"))
	if err != nil {
		return errors.Wrap(err, "deletewebhook: post request failed")
	}

	data := deleteWebhookResponse{}
	err = resp.ToJSON(&data)
	if err != nil {
		return errors.Wrap(err, "deletewebhook: to json failed")
	}
	if !data.OK {
		return errors.New(fmt.Sprintf("deletewebhook: description='%s'", data.Description))
	}

	return nil
}
