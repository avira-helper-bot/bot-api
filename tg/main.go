package tg

import (
	"fmt"
	"gitlab.com/avira-helper-bot/bot-api/config"
)

func getTelegramUrl(method string) string {
	return fmt.Sprintf("https://api.telegram.org/bot%s/%s", config.BotApiToken, method)
}
